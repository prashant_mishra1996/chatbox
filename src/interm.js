import React from 'react'
import './App.css'
import App from './apple';
import Layout from './Layouts/Layout';

class Interm extends React.Component{
    
    state={
        rooms:[],
        isActive:false,
        name:''
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let rooml=this.state.rooms;
        rooml.push(this.state.name);
        this.setState({
            rooms:rooml,
            name:''
        })
    }
    handleChange=(e)=> {
        this.setState({
        [e.target.name]: e.target.value
      });
    }

    render(){
            let view=''
        if(this.state.rooms.length>0){
            this.state.rooms.map((name)=>{
                 view=(
                 <div>
                <Layout rooms={this.state.rooms}>
                <App name={name}/>
                </Layout>
                

                 </div>)
                
            })}
        
        
        return(
            <div class="Container">
                <div>{view}</div>
                <h2> Create Room</h2>
                
                <form onSubmit={this.handleSubmit} >
            
                <input type="text" name="name" className="TextBox" value={this.state.name} onChange={this.handleChange} />
          
                <input type="submit" className="Send"></input>
                </form>
               
            </div>
            
           
        )
        }
}
export default Interm;