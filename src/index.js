import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Interm from './interm';
import App from './App';
import  {createStore} from 'redux';
import {Provider} from 'react-redux'

const reducer=(state,action)=>{
    switch(action.type){
        case "Add Messages":
            state={
                ...state,
            }
            state.chats[state.currentid].messages.push(state.message)
            state.message=""
            break;
        case "Add Chat":
            state={
                ...state
                }
            state.chats.push({
                id:state.idcount,
                name:action.payload,
                messages:[]
            })
            state.idcount+=1;
            break;
        case "Set Current":
            state={
                ...state,
                current:action.payload.m,
                currentname:action.payload.n,
                currentid:action.payload.p
            }
            break;
        case "HandleChange":
            state={
                ...state,
                message:action.payload
            }
            break;
            
    }
    return state;
}

const store = createStore(reducer,{
    chats:[
        {   id:0,
            name:'Prashant',
            messages:['hi','How are you','what are you doing'],
            
        },
        {   id:1,
            name:'Jasmeet',
            messages:['hi','what are you doing'],
        },
        {
            id:2,
            name:'Arjit',
            messages:['hi guys','listening to something']
        }
    ],
    current:[],
    currentname:'',
    currentid:0,
    message:'',
    idcount:3
})

store.subscribe(()=>{
    console.log("store updated",store.getState());
})






ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));
