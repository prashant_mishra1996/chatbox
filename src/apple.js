import React from 'react';
import MessageBox from './Components/MessageBox/MessageBox';
import './App.css';

class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            messages:[],
            message:'',
                     

        }
        this.handleChange=this.handleChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);

    }
    handleChange(e) {
        this.setState({
        [e.target.name]: e.target.value
      });
  }

  handleSubmit(e) {
      e.preventDefault();

      let messagel=this.state.messages;
      messagel.push(this.state.message)
          this.setState({
             messages: messagel,
             message:""
          })
          

      }
  
    
    render(){
        return(
            <div>
                <div>
                    <MessageBox messagesw={this.state.messages}/>
                </div>
                    
                <center>
                
                <form   onSubmit={this.handleSubmit} className="FormFields" >
            
                <input className="TextBox" type="text" name="message" value={this.state.message} onChange={this.handleChange} />
          
                <input type="submit" className="Send"></input>
                </form>
            </center>
                
            </div>
        )
    }
}
export default App;