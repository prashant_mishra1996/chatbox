import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter ,InputGroup, InputGroupAddon, Input} from 'reactstrap';
import './Modal.css';

class ModalExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      Addname:''
      
    };

    this.toggleCancel = this.toggleCancel.bind(this);
    this.toggelSave = this.toggelSave.bind(this);
  }

  toggleCancel() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  toggelSave(){
    this.setState(prevState => ({
      modal: !prevState.modal,
      Addname:''
    }));

    {this.props.addChat(this.state.Addname)}
  }
  handleChange=(e)=> {
    this.setState({
    [e.target.name]: e.target.value
  });
}
  render() {
      console.log(this.props.addChat)
    return (
      <div>
        <Button color="secondary" style={{backgroundColor:'#F2F2F2',color:'#B81E1F',border:'none',fontWeight:"bold"}} onClick={this.toggleCancel}>{this.props.buttonLabel}</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle} charCode="Y">Create New Chat</ModalHeader>
          <ModalBody>
          <InputGroup>
        <Input name="Addname" value={this.state.Addname} onChange={this.handleChange}/>
      </InputGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggelSave}>Create Chat</Button>{' '}
            <Button color="secondary" onClick={this.toggleCancel}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default ModalExample;