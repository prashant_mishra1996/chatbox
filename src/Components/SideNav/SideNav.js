import React from 'react';
import './SideNav.css';
import NavItem from './navItem/navItem'
import Tabs from './Tabs/tabs';
import ModalExample from './Modal/Modal'

 const SideNav=(props)=>
 {  console.log(props.addChat)
     var a=props.chats.map((i)=>{
     return(<div key={i.id} onClick={()=>{props.changeCurrent(i.messages,i.name,i.id)}}><NavItem name={i.name}></NavItem></div>);
     

  
 })
     return(
         
     <div className="sideNavigation">
         <Tabs></Tabs>
         <div className="room">
             <div className="plus">+</div>
             <div className="plustext"><ModalExample buttonLabel="Start New Chat" addChat={props.addChat}></ModalExample></div>
         </div>
         <div className="list">{a}</div>
         
     </div>)
 }
 export default SideNav; 