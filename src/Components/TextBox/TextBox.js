import React from 'react';
class TextBox extends React.Component{
    constructor(props){
        super(props);
    }
    state={
        textValue:''
    }
    handleChange=(e)=> {
        this.setState({
        [e.target.name]: e.target.value
      });
    }
    
    render(){
        return(
            <div class="Container">
                
                <form onSubmit={()=>{this.props.addMessage(this.state.textValue)}} >
            
                <input type="text" name="textValue" className="TextBox" value={this.state.textValue} onChange={this.handleChange} />
                <input type="submit" className="Send"></input>
                </form>
               
            </div>
        )
    }
}
export default TextBox;