import React from 'react';
import Layout from './Layouts/Layout';
import MessageBox from './Components/MessageBox/MessageBox';
import{connect} from 'react-redux'

class App extends React.Component{
    constructor()
    {
        super();
        // this.state={
        //     message:''
        // }
    }
//     AddChat=(name)=>{
//         let c=this.state.chats
//         c.push({
//             id:this.state.idcount,
//             name:name,
//             messages:[]
//         })
//         this.setState({
//             chats:c
//         })

//     }
    // handleChange=(e)=> {
    //     this.setState({
    //     [e.target.name]: e.target.value
    //   });
      
//   }
//     AddMessage=(e)=>{
//         e.preventDefault();
//         let currentidcopy=this.state.currentid
//         let chatcopy=this.state.chats;
//         console.log(chatcopy);
//         chatcopy[currentidcopy].messages.push(this.state.message)
//         this.setState({
//             chats:chatcopy,
//             message:''
//         })
//         console.log(chatcopy);
        
//     }
//     changeCurrent=(m,n,p)=>{
//         this.setState({
//             current:m,
//             currentname:n,
//             currentid:p
//         })
//         console.log(m,n,p)
       
//     }
    render(){
        return(
            <div className='App'>
                <Layout chats={this.props.chats} changeCurrent={this.props.changeCurrent} addChat={this.props.AddChat}><MessageBox messagesw={
                this.props.current} name={this.props.currentname}></MessageBox>
            
                </Layout>
                <form   onSubmit={this.props.AddMessage} className="FormFields" >
            
            <input className="TextBox" type="text" placeholder="Enter Message Here" name="message" value={this.props.message} onChange={this.props.handleChange} />
      
            </form>
            </div>
            
            
        )
    }
}
    const mapStateToProps=(state)=>{
        return{
            chats:state.chats,
            current:state.current,
            currentid:state.currentid,
            message:state.message,
            currentname:state.currentname,
            idcount:state.idcount
        }  
    }
    const mapDispatchToProps=(dispatch)=>{
        return{
            changeCurrent:(m,n,p)=>{
                dispatch({
                    type:"Set Current",
                    payload:{
                        m:m,
                        n:n,
                        p:p
                    }
                });
            },
            AddChat:(name)=>{
                dispatch({
                    type:"Add Chat",
                    payload:name
                })

            },
            handleChange:(e)=>{
                dispatch({
                    type:"HandleChange",
                    payload:e.target.value
                })

            },
            AddMessage:(e)=>{
                e.preventDefault();
                dispatch({
                    type:"Add Messages",
                    payload:''
                })

            },
            

        }
    }
 export default connect(mapStateToProps,mapDispatchToProps)(App);
