import React from 'react';
import './Layout.css';
import SideNav from '../Components/SideNav/SideNav'



const Layout=(props)=>{
    return(
        
        <div>
            <div className="SideNav">
                <SideNav chats={props.chats} changeCurrent={props.changeCurrent} addChat={props.addChat}/>
            </div>
            <div className="MessageBox">
                {props.children}
            </div>
        </div>
            
            
            
            
        
    );
}
export default Layout;